var CalcBase = Base.extend({
    constructor: function(el){
        this.el = el;
    },
    show: function(){
        this.el.show();
    },
    linkInputLabel: function(input, label){
        var self = this;
        input.bind('input', function(){ self.set(parseInt($(this).val()), $(this).attr('name'), label); });
    },
    set: function(val, label){
        if (this.isValid(val))
            label.text(Calc.toReadable(val));
    },
    isValid: function(val){
        return !isNaN(val) && val >= Calc.minSize && val <= Calc.maxSize;
    }
});
var Calc = Base.extend({
    constructor: function(){
        this.menu_items = $('.calculate-types-item');
        this.calcs = {};

        var self = this;
        this.menu_items.each(function(){
            var type = $(this).attr('data-name');
            var calc = $('.calculate-content_' + type);
            if (calc.length) {
                self.calcs[type] = new Calc[type](calc);
                $(this).click(function(){
                    self.select(type, $(this));
                });
                if (type == Calc.selectCalc)
                    self.select(type, $(this));
            }
        });
        if (this.menu_items.length == 1)
            $('.calculate-types').hide();
    },
    select: function(type, el){
        if (!el.hasClass('active')) {
            $('.calculate-content').hide();
            this.menu_items.removeClass('active');
            this.calcs[type].show();
            el.addClass('active');
        }
    }
},{
    toReadable: function (n) {
        return (n + "").replace(/\B(?=(\d{3})+(?!\d))/g, " ");
    },
    Radio: Base.extend({
        constructor: function(el, form){
            this.el = el;
            this.form = form;
            this.name = this.el.attr('data-name');
            this.items = this.el.find('.calculate-radio-item');
            var self = this;
            this.items.click(function(){
                self.items.removeClass('active');
                self.value = $(this).attr('data-value');
                $(this).addClass('active');
                self.form.set(self.value, self.name, false);
            });
            this.value = this.items.filter('.active').attr('data-value');
            this.form.data[this.name] = this.value;
        }
    }),
    minSize: 1000,
    maxSize: 10000
});
Calc.window = CalcBase.extend({
    constructor: function(el){
        this.base(el);
        this.types = this.el.find('.calc-types .el');
        this.labelWidth = this.el.find('.calc-labelWidth');
        this.labelHeight = this.el.find('.calc-labelHeight');
        this.inputWidth = this.el.find('.calc-inputWidth');
        this.inputHeight = this.el.find('.calc-inputHeight');
        this.price = this.el.find('.calc-price');

        var self = this;
        this.types.click(function(){ self.select($(this).attr('data-type')); });
        this.linkInputLabel(this.inputWidth, this.labelWidth);
        this.linkInputLabel(this.inputHeight, this.labelHeight);

        this.select(Calc.window.selectType);
    },
    select: function(type){
        this.inputWidth.bind('input');
        this.inputHeight.bind('input');
        this.curType = type;
        for (var tp in Calc.window.data) {
            this.el.removeClass('calculate_' + tp);
        }
        this.el.addClass('calculate_' + this.curType);
        this.types.removeClass('active').filter('[data-type="'+this.curType+'"]').addClass('active');
        this.update(true);
    },
    update: function(writeInput){
        var cur = Calc.window.data[this.curType];

        this.labelHeight.text(Calc.toReadable(cur.height));
        if (writeInput) {
            this.inputWidth.val(cur.width);
            this.inputHeight.val(cur.height);
        }
        var price = Math.round(cur.width / 1000 * cur.height / 1000 * cur.price);
        this.price.text(Calc.toReadable(price));
    },
    set: function(val, type, label){
        if (this.isValid(val)) {
            this.base(val, label);
            Calc.window.data[this.curType][type] = val;
            this.update(false);
        }
    }
}, {
    data: {
        zero: {
            price: 2500,
            width: 1000,
            height: 1200
        },
        one: {
            price: 4200,
            width: 1000,
            height: 1200
        },
        two: {
            price: 4000,
            width: 1400,
            height: 1200
        },
        three: {
            price: 3300,
            width: 1600,
            height: 1400
        }
    },
    selectType: 'two'
});
Calc.block = CalcBase.extend({
    constructor: function(el){
        this.base(el);
    }
});
Calc.balcony = CalcBase.extend({
    constructor: function(el){
        this.base(el);
        this.labelWidth     = this.el.find('.calc-labelWidth');
        this.labelHeight    = this.el.find('.calc-labelHeight');
        this.inputWidth     = this.el.find('.calc-inputWidth');
        this.inputHeight    = this.el.find('.calc-inputHeight');
        this.inputDeep      = this.el.find('.calc-inputDeep');
        this.price          = this.el.find('.calc-price');
        this.data           = {};
        this.typePrices     = Calc.balcony.prices;

        this.radios             = this.el.find('.calculate-radio');
        this.initRadio('tipRadio',          'tip');
        this.initRadio('bokovyeRadio',      'bokovye');
        this.initRadio('panoramnoeRadio',   'panoramnoe');
        this.initRadio('uteplenieRadio',    'uteplenie');
        this.initRadio('saidingRadio',      'saiding');
        this.initRadio('vagonkaRadio',      'vagonka');
        this.initRadio('kryshaRadio',       'krysha');

        this.linkInputLabel(this.inputWidth, this.labelWidth);
        this.linkInputLabel(this.inputHeight, this.labelHeight);
        this.linkInputLabel(this.inputDeep, false);

        this.inputWidth.val(3000).trigger('input');
        this.inputHeight.val(2000).trigger('input');
        this.inputDeep.val(1000).trigger('input');

        this.update();
    },
    initRadio: function(varName, dataName){
        var radio = this.radios.filter('[data-name="'+dataName+'"]');
        if (radio.length != 0) {
            this[varName] = new Calc.Radio(radio, this);
        } else {
            this.data[dataName] = false;
        }
    },
    update: function(){
        if (this.data.height - Calc.balcony.parapet < 500) return false;

        // WINDOW PRICE
        var parapetHeight = this.data.panoramnoe == 'no' ? Calc.balcony.parapet : 0;
        var winHeight = this.data.height - parapetHeight;
        var sideWinSquare = parseInt(this.data.bokovye) * winHeight/1000 * this.data.deep/1000;

        var homeWallSquare  = this.data.width/1000 * Calc.balcony.homeWall * this.data.height/1000;
        var bottomSquare    = parapetHeight/1000 * (this.data.width/1000 + this.data.deep/1000 * 2);
        var outsideSquare   = this.data.isLoggia ? parapetHeight/1000 * this.data.width/1000 : bottomSquare;
        var sidesSquare     = (2 - parseInt(this.data.bokovye)) * winHeight/1000 * this.data.deep/1000;
        var insideSquare    = homeWallSquare + bottomSquare + sidesSquare;
        var roofSquare      = this.data.width/1000 * this.data.deep/1000;

        var windowPrice     = (this.data.width/1000 * winHeight/1000 + sideWinSquare) * this.typePrices[this.data.tip];
        var utepleniePrice  = this.data.uteplenie == 'yes' ? (bottomSquare + sidesSquare) * this.typePrices.uteplenie : 0;
        var saidingPrice    = this.data.saiding == 'yes' ? outsideSquare * this.typePrices.saiding : 0;
        var vagonkaPrice    = this.data.vagonka == 'yes' ? insideSquare * this.typePrices.vagonka : 0;
        var kryshaPrice     = this.data.krysha == 'yes' ? roofSquare * this.typePrices.krysha : 0;

        var price = Math.round(windowPrice + utepleniePrice + saidingPrice + vagonkaPrice + kryshaPrice);
        this.price.text(Calc.toReadable(price));
    },
    set: function(value, name, label){
        this.data[name] = value;
        this.update();
        if (label)
            this.base(value, label);
    }
}, {
    parapet: 900,
    homeWall: 0.25
});
Calc.loggia = Calc.balcony.extend({
    constructor: function(el){
        this.base(el);
        this.radios.filter('[data-name="bokovye"]').hide();
        this.radios.filter('[data-name="krysha"]').hide();
        this.data.bokovye = '0';
        this.data.krysha = 'no';
        this.data.isLoggia = true;
        this.typePrices = Calc.loggia.prices;
        this.update();
    }
});