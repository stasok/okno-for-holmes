var Tabs = Base.extend({
    constructor: function(groups){
        this.groups = groups;
        this.groupsArr = [];
        var self = this;
        this.groups.each(function(){
            self.groupsArr.push(new Tabs.Group($(this)));
        });

        // Comment because PageSpeed low
        /*this.cache = $("#cache");
        $(window).load(function(){
            for (var type in Tabs.libs){
                for (var item = 0; item < Tabs.libs[type].length; ++item) {
                    self.cacheImg(Tabs.libs[type][item]['icon']);
                }
            }
        });*/
    }
    /*cacheImg: function(src){
        $("<img src='"+ (Tabs.imgDir + src) +"' />").appendTo(this.cache);
    }*/
}, {
    Group: Base.extend({
        constructor: function(group){
            this.el = group;
            this.lib = Tabs.libs[this.el.data('lib-id')];
            this.count = this.lib.length - 1;
            this.icon   = this.el.find('.tabs .icon img');
            this.title  = this.el.find('.tabs .title');
            this.desc   = this.el.find('.tabs .desc');
            this.prev   = this.el.find('.control .l');
            this.next   = this.el.find('.control .r');

            var self = this;
            if (this.count > 0) {
                this.prev.show();
                this.next.show();
                this.prev.click(function(){ self.onPrev(); });
                this.next.click(function(){ self.onNext(); });
            }

            $(window).load(function(){
                self.set(0);
            });
        },
        set: function(num){
            var data = this.lib[this.cur = num];
            this.icon.attr('src', Tabs.imgDir + data.icon);
            this.title.text(data.title);
            this.desc.html(data.desc);
        },
        onPrev: function(){
            this.set(this.cur == 0 ? this.count : this.cur - 1);
        },
        onNext: function(){
            this.set(this.cur == this.count ? 0 : this.cur + 1);
        }
    }),
    imgDir: '../global/images/tabs/',
    libs: {
        brands: [],
        windows: [],
        frames: [],
        fittings: []
    }
});