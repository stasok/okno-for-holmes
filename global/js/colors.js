var Colors = Base.extend({
    constructor: function(el){
        this.el = el;
        this.list = this.el.find('.els');
        this.bigImg = this.el.find('.image');
        this.label = this.el.find('.clabel');

        var self = this;
        this.el.find('.l').click(function(){ self.prev(false); });
        this.el.find('.r').click(function(){ self.next(false); });

        this.init();

        // Comment because PageSpeed low
        /*this.cache = $("#cache");
        $(window).load(function(){
            for (var i = 0; i < Colors.lib.length; ++i) {
                self.cacheImg(i + 1);
            }
        });*/
    },
    /*cacheImg: function(pic_id){
        $("<img src='"+ Colors.imgDir + pic_id + ".jpg' />").appendTo(this.cache);
        $("<img src='"+ Colors.imgDir + "m_" +pic_id +".png' />").appendTo(this.cache);
    },*/
    init: function(){
        for (var i = 1; i <= Colors.count; ++i)
            this.appendItem(i);

        var self = this;
        $(window).load(function(){
            self.setCurrent(1);
        });
    },
    select: function(pic_id){
        this.setCurrent(pic_id);
        var els = this.list.find('.el');
        for (var i = 0; i < els.length; ++i) {
            var pId = parseInt($(els[i]).attr('data-picId'));
            if (pId == pic_id) {
                this[i > 1 ? 'next' : 'prev'](true);
                break;
            }
        }
    },
    next: function(withoutCurrent){
        var lastPicId = this.list.find('.el:last').attr('data-picId');
        var nextPicId = lastPicId != Colors.lib.length ? parseInt(lastPicId) + 1 : 1;
        this.appendItem(nextPicId);

        var self = this;
        this.list.animate({ left: '-60px' }, { duration: 250, complete: function(){
            if (!withoutCurrent)
                self.setCurrent(parseInt(self.getId(self.curPicId).next().attr('data-picId')));
            self.list.find('.el:first').remove();
            self.list.css({ left: 0 });
        } });

    },
    prev: function(withoutCurrent){
        var firstPicId = this.list.find('.el:first').attr('data-picId');
        var prevPicId = firstPicId != 1 ? parseInt(firstPicId) - 1 : Colors.lib.length;
        this.prependItem(prevPicId);
        this.list.css({ left: '-60px' });

        var self = this;
        this.list.animate({ left: '0' }, { duration: 250, complete: function(){
            if (!withoutCurrent)
                self.setCurrent(parseInt(self.getId(self.curPicId).prev().attr('data-picId')));
            self.list.find('.el:last').remove();
        } });

    },
    setCurrent: function(pic_id){
        this.curPicId = pic_id;
        this.list.find('.el').removeClass('active');
        this.getId(pic_id).addClass('active');
        var bigImg = this.bigImg;
        this.bigImg.stop().animate({ opacity: 0 }, {
            duration: 200,
            complete: function(){
                bigImg.attr('src', Colors.imgDir + pic_id + '.jpg').load(function(){
                    bigImg.stop().animate({ opacity: 1 }, { duration: 200 });
                });
            }
        });
        this.label.text(Colors.lib[pic_id - 1]);
    },
    appendItem: function(pic_id){
        this.getItem(pic_id).appendTo(this.list);
    },
    prependItem: function(pic_id){
        this.getItem(pic_id).prependTo(this.list);
    },
    getItem: function(pic_id){
        var thumbUrl = Colors.imgDir + 'm_' + pic_id + '.png';
        var item = $("<div class='el'><div class='border'></div></div>").css({ backgroundImage: 'url(' + thumbUrl + ')' }).attr('data-picId', pic_id);
        var self = this;
        return item.click(function(){
            if (!$(this).hasClass('active'))
                self.select($(this).attr('data-picId'));
        });
    },
    getId: function(pic_id){
        return this.list.find('.el[data-picId="' + pic_id + '"]');
    }
},{
    imgDir: '../global/images/colors/',
    count: 4,
    lib: [
        'Розовая берёза',
        'Орегон',
        'Горная сосна',
        'Светлый дуб',
        'Орех',
        'Золотой дуб',
        'Зимний дуглас',
        'Дуглас',
        'Натуральный орех',
        'Грецкий орех',
        'Вишня амаретто',
        'Орех бальзамико',
        'Золотой орех',
        'Дуб',
        'Дуб 2',
        'Тёмный дуб',
        'Морёный дуб',
        'Махагон',
        'Чёрная вишня',
        'Морёный дуб 2',
        'Серый тик',
        'Серебристый металлик',
        'Белый структурный',
        'Кремовый',
        'Светло-серый',
        'Серый стальной',
        'Серый',
        'Антрацит серый',
        'Красный',
        'Тёмно-красный',
        'Бордовый',
        'Красно-коричневый',
        'Бриллиантово-голубой',
        'Тёмно-синий',
        'Светло-зелёный',
        'Зелёный',
        'Тёмно-зелёный',
        'Чёрно-коричневый',
        'Шоколадно-коричневый'
    ]
});