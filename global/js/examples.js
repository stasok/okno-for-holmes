var Examples = Base.extend({
    constructor: function(el){
        this.el = el;
        this.img = this.el.find('.cimage img');
        this.title = this.el.find('.right .top');
        this.from = this.el.find('.i .from');
        this.of = this.el.find('.i .of');

        var self = this;
        this.el.find('.scontrol .l').click(function(){ self.prev(); });
        this.el.find('.scontrol .r').click(function(){ self.next(); });
        this.count = Examples.items.length;
        this.of.text(this.count);

        $(window).load(function(){
            self.select(0);
        });

        // Comment because PageSpeed low
        /*this.cache = $("#cache");
        $(window).load(function(){
            for (var i = 1; i <= Examples.items.length; ++i){
                self.cacheImg('ex_' + i + '.jpg');
            }
        });*/
    },
    /*cacheImg: function(src){
        $("<img src='"+ (Examples.imgDir + src) +"' />").appendTo(this.cache);
    },*/
    prev: function(){
        var num = this.cur != 0 ? this.cur - 1 : this.count - 1;
        this.select(num);
    },
    next: function(){
        var num = this.cur != this.count - 1 ? this.cur + 1 : 0;
        this.select(num);
    },
    select: function(num){
        this.cur = num;
        var img = this.img;
        var item = Examples.items[num];
        this.img.stop().animate({ opacity: 0 }, {
            duration: 200,
            complete: function(){
                img.attr('src', Examples.imgDir + item.id + '.jpg').load(function(){
                    img.animate({ opacity: 1 }, { duration: 200 });
                });
            }
        });
        this.title.text(item.desc);
        this.from.text(num + 1);
    }
}, {
    imgDir: '../global/images/examples/'
});