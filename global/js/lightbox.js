var Lightbox = Base.extend({
    constructor: function(el){
        this.el = el;
        this.name = this.el.data("name");
        this.close = this.el.find(".lightbox-close");
        this.fix = $(".lightbox-fix");
        this.body = $(document.body);
        this.win = $(window);
        this.overlay = $(".lightbox-overlay");
        this.isShow = false;

        var self = this;
        this.close.click(function(){ self.hide(); });
        this.win.resize(function(){ if (self.isShow) self.position(); });

        this.overlay.click(function(){ self.hide(); });
        this.el.click(function(e){ e.stopPropagation(); });
        this.onClose = function(){};
    },
    show: function(){
        this.isShow = true;
        this.scrollTop = this.win.scrollTop();
        this.body.addClass("lightboxMode");
        this.fix.css({ top: (-this.scrollTop) + "px" });
        this.win.scrollTop(0);
        this.el.fadeIn(250);
        this.position();
    },
    hide: function(){
        this.isShow = false;
        this.body.removeClass("lightboxMode");
        this.el.hide();
        this.win.scrollTop(this.scrollTop);
        this.onClose();
    },
    position: function(){
        this.el.css({ marginTop: Math.max((this.win.height()-80-this.el[0].offsetHeight)/2, 0) });
    },
    setLoad: function(){
        this.el.addClass("lightbox_load");
    },
    unsetLoad: function(){
        this.el.removeClass("lightbox_load");
    }
});