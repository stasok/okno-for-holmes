var Callback = {
    action: function(phoneNumber, timestamp){
        if (Callback.metrikaCounter != "") {
            window[Callback.metrikaCounter].reachGoal(timestamp ? 'cta_longCallback' : 'cta_callback');
        }
        $.ajax({
            url: "http://pbx.doublesales.ru/callback_to_crm.php",
            method: "POST",
            data: {
                source: Callback.crmSource,
                phone: phoneNumber,
                timestamp: parseInt(timestamp) > 1262286000 ? timestamp : "false",
                url: encodeURIComponent(location.href),
                tracking: $.cookie("tracking")
            },
            crossDomain: true,
            datType: "jsonp"
        });
    },
    metrikaCounter: "",
    crmSource: ""
};