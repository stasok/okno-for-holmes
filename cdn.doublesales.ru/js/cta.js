var CTA = Base.extend({
    constructor: function(el, controller){
        this.el = el;
        this.controller = controller ? controller : false;
        this.name = this.el.data("name");
        this.phone = new CTA.Phone(this.el.find(".cta-field"), this);
        this.button = this.el.find(".cta-submit");
        this.loader = this.el.find(".cta-loader");
        this.thanks = this.el.find(".cta-thanks");
        this.form = this.el.find(".cta-form");

        var self = this;
        this.button.click(function(){
            self.action();
            return false;
        });
        if ($.cookie(this.name) == "true") {
            this.form.hide();
            this.thanksMode(true);
        }
        this.el.addClass("cta_inited");
    },
    action: function(){
        this.phone.isCorrect ? this.send() : this.warn();
    },
    send: function(){
        this.form.hide();
        this.loadMode();
        var self = this;

        $.ajax({
            url: CTA.baseUrl + "send.order.php",
            method: "POST",
            data: this.getData(),
            success: function(){
                self.thanksMode();
            }
        });
    },
    getData: function(){
        var data = {
            url: encodeURIComponent(location.href),
            tracking: $.cookie("tracking")
        };
        this.el.find("input").each(function(){
            var input = $(this);
            data[input.attr("name")] = input.val();
        });
        data.type = this.name;
        data.adv_phone = window.advPhone ? window.advPhone : '';
        if (CTA.id) {
            data.id = CTA.id;
        }
        return data;
    },
    thanksMode: function(ignoreNew){
        this.loader.hide();
        this.el.removeClass("cta_load").addClass("cta_thanks");
        this.thanks.fadeIn(250);

        if (!ignoreNew) {
            var typeInput = this.el.find('input[name="'+this.name+'-dynamicType"]');
            var eventName = typeInput.length != 0 ? typeInput.val() : this.name;
            $.cookie(this.name, "true", { expires: 1 });
            if (CTA.metrika != "") {
                window[CTA.metrika].reachGoal('cta_' + eventName);
            }
        }
        if (this.controller)
            this.controller.ctaSuccess(this.el);
    },
    loadMode: function(){
        this.el.addClass("cta_load");
        this.loader.hide().fadeIn(250);
    },
    warn: function(){
        var l = 6;
        for( var i = 0; i <= 5; i++ )
            this.phone.container.animate( { left: (i == 5 ? 0 : ( l = -l )) + 'px' }, 50);
    }
}, {
    Phone: Base.extend({
        constructor: function(el, cta){
            this.el = el;
            this.cta = cta;
            this.isExtStyle = this.el.find(".cta-phone-input").length == 0;
            this.container = !this.isExtStyle ? this.el.parent() : this.el;
            this.field = !this.isExtStyle ? this.el.find(".cta-phone-input") : this.el;
            this.isCorrect = false;

            var self = this;
            this.el.click(function(){
                self.focus();
            });
            this.field.blur(function(){
                self.check();
                self.blur();
            }).on("input", function(){
                self.check();
            });

            this.field.keydown(function(e) {
                // Отправляем по Enter
                if (e.keyCode == 13) {
                    self.cta.action();
                    return true;
                }

                // Разрешаем нажатие клавиш backspace, del, tab и esc
                if ( e.keyCode == 46 || e.keyCode == 8 || e.keyCode == 9 || e.keyCode == 27 ||
                        // Разрешаем выделение: Ctrl+A
                    (e.keyCode == 65 && e.ctrlKey === true) ||
                        // Разрешаем клавиши навигации: home, end, left, right
                    (e.keyCode >= 35 && e.keyCode <= 39)) {
                }
                else {
                    // Запрещаем всё, кроме клавиш цифр на основной клавиатуре, а также Num-клавиатуре
                    if ((e.keyCode < 48 || e.keyCode > 57) && (e.keyCode < 96 || e.keyCode > 105 )) {
                        e.preventDefault();
                    }

                }
            });

            this.check();
        },
        focus: function(){
            if (!this.isExtStyle)
                this.el.addClass("cta-phone_focused");
            if (!this.isCorrect)
                this.field.focus();
        },
        blur: function(){
            if (!this.isExtStyle)
                this.el.removeClass("cta-phone_focused");
        },
        check: function(){
            var phone = this.field.val().replace(/[^0-9]/g, '');
            if (phone.length > 6)
                this.correct();
            else if (phone.length < 1)
                this.empty();
            else this.typed();
        },
        correct: function(){
            this.isCorrect = true;
            if (!this.isExtStyle)
                this.el.addClass("cta-phone_correct");
        },
        typed: function(){
            this.isCorrect = false;
            this.isTyped = true;
            if (!this.isExtStyle) {
                this.el.removeClass("cta-phone_correct");
                this.el.addClass("cta-phone_typed");
            }
        },
        empty: function(){
            this.isCorrect = false;
            this.isTyped = false;
            if (!this.isExtStyle) {
                this.el.removeClass("cta-phone_correct");
                this.el.removeClass("cta-phone_typed");
            }
        }
    }),
    metrika: "",
    domain: "",
    baseUrl: ""
});